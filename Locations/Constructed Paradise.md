# Constructed Paradise

The constructed paradise is an area maintained by a group of magicians.
They are the ones breeding new humans to live in a huge area, since the humans can't reproduce themselves.
There's only one kind of humans. All of them are basically females and they don't die from age. They are called perfect humans.

There are no other animals besides humans. So cannibalism is common in some cultures.
And there's only one edible plant, who has been artificially created and grows almost everywhere.

The magicians created some gods to watch for the different areas.
The gods are huge creatures having various appearances and abilities related to the area they live in.
In total there are 43 gods.

There are different barriers to prevent the humans to leave the area cultivated area.
The most important barriers are the gods, especially the three gods enclosing the area.
But there's also a strong electro magnetic field to prevent electric devices to work when getting close to the border.

