# Island Area

The island area is an area in the middle of the ocean. There are a few huge islands where most people live and many small islands around.

The next continent is hundreds of kilometers away and it's believed that nobody lives there anymore.
So most people don't leave this place.

There are three huge groups of people, who share the main island.
There are the technology enthusiasts, who build a huge city on a wasteland peninsula and live there.
There are a bunch of conservative people, who mostly live on a huge meadow peninsula in different villages.
And there is a huge forest peninsula, where a few dropouts and nature lovers settle to live some alternative lifestyles.

The technology enthusiasts mostly stay in their safe city where they create a lot of new technology and also new intelligent humanoid races which quickly spread in different places.
They export their technology to other places, especially to villages, and do some vacations in other places, where they often act as superior.

The conservatives explore many places in the world, even small islands far away from the  to settle there and trade.
They also develop common traditions and build hierarchical structures to solve their problems.

The nature people stay in their small tribes and communes and have a simple and content life.
Some of them have questionable rituals including torture, sex and death.
City people like to use them as personal slaves and village people sometimes use them as cheap workers and helpers.

