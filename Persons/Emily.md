# Emily

Emily is the only human who didn't die after getting infected by the K1 virus.
She has the power to genetically modify every living thing including herself.
This way she can easily give and take life and let creatures follow her commands.

She doesn't have full control about her power. Even if she tries to, she is not able to die.

## Life

Emily grows up with her family in a shack in a on an island close to the city.
She has both parents and an older brother Jakob.

At a pretty young age, she is attacked by a pet they got from the biosphere.
She has a hole in her belly and her spine is almost cut through, but she doesn't die.

After professional medication in a hospital she seems to heal. Then it looks like she died and she gets buried.

Years later she gets out of her grave still being alive. She doesn't know, what happened.
She finds out about her new powers. She can now influence living creatures.
She can take the life out of plants and let them grow like she wants by touching them.
She can also control animals. And she learns how to heal and modify her own body.

When meeting her brother Jakob again, she is really happy. She falls in love with him and wants sexual contact.
Jakob feels uncomfortable about the idea, even after she explains how their children would be healthy.
Because of the rejection, she gets mad and wants to destroy everything.

Her powers are too strong and nobody can beat her, even magic and modern weapons fail.
In the end, another guy Taj gives up his own relationship to start a relationship with her, which calms her down.

For some time they have a happy relationship and even get a daughter Ilaya together. Ilaya doesn't seem to age after a few years, but she doesn't seem to have any powers.
When Taj gets older she has to transform herself manually since she doesn't age anymore.
She also tries to prevent Taj from death, and for some decades this works, but he starts to get sad, and one day her home is attacked.
Taj dies. She almost dies. Ilaya doesn't die, but Emily believes she died.

After some time she wakes up in an unknown place. She doesn't know how much time has passed. Probably years.
Now she finally realizes she cannot die. Without Taj, she doesn't want to live anymore. She tries to kill herself multiple times but it doesn't work.
So she has to accept this fact and decides to mess with her thoughts.

From now she just wants to live life as it comes.
For centuries she has nice adventures with different people. There are times, where she's very well known, there are times where she wants to help, and there are times, where she's just a danger to everyone. And sometimes she almost dies and doesn't wake up for years.

Sometimes she forgets almost everything and becomes a totally new person.

Even when the world is almost distinguised, she doesn't die. Then she has to live with only plants and weird creatures around.
This makes her sad and she'd like to die again.
When some humans who live in some space station come back to earth, she likes to mess around with them. But it's always just single humans and they die soon, so she's alone again for years or decades. She tries not to get too attached.

She is still there when people get back to earth to settle there and plays a major role for that.

One day, she meets Ilaya again. She doesn't really remember her, but now she will never have to be alone again, even when humanity gets extinguished.

