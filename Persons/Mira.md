# Mira

Mira is a is a very powerful magician.
She is able to master all kinds of energy magic and space time magic easily.
But she just likes to have a peaceful life by herself.

## Life

Mira grows up like most girls in the early days of the constructed paradise.
She is raised by random people who find her one day and then stays at this place for centuries.
She also has a sister Nera, and they do almost everything together.

Some day she starts to travel around.
During her journey she learns a bunch of new abilities and builds a bunch of habits like reading, fighting and cooking.
Nera is always on her side.

After some more centuries she ends up in the great valley.
She finds a new home there, where she lives with different people, and finds many new friends there.

After living here for most of her life, people come and go, but her best friend who lives there almost as long as her is Elena.
One day Nera is killed by another friend and flatmate Krista because she was hungry.
Mira was a bit sad, but she knows that's just how things are here.

When Elena leaves not many years later to go to death mountain, Mira also leaves her home. She can't remember living anywhere else.

She moves to a close home, where a friend of her, Resa, lives. She's a scientist and has found a boy, something both of them never have seen.

He's normally naked and looks a bit different than other people. Resa tries to find out more about him.

In the end, they find out about his origins.
It's difficult, but in the end, they manage to get there.
They learn about how the world works and where new humans even come from.
And she meets the magicians, who control that world. She stays there and learns a lot.
She stays there for centuries and becomes one of the most powerful magicians.
But when she is involved in disputes she is not interested in, she flees.

She hides alone far away from civilization where she builds a small shack.
There she continues her life just like before and tries to follow her old habits again, more or less regularly.
Reading, fighting, cooking, and a lot of relaxing.
Every few certuries she gets visitors, but most of the time, they don't stay too long.

But one day she is discovered by hostile magicians, so she has to flee again.
In the end, she is able to solve her problems and get back to her simple life.
But since she found new friends again and keeps in touch with them.

