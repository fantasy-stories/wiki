This is a wiki about a fantasy world.
It takes place in a futuristic world after humanity almost got extinct and only a few regions on earth still contain humans.
It could be the future of our world, but there are no explicit references to indicate that.
The world has a long history spanning a time of maybe thousands of years. There are no specific durations of these phases.

There are some unrelated main areas:

- [Island Area](Locations/Island Area.md)
- [Artificial Paradise](Locations/Constructed Paradise.md)
- [Lepin Planet](Locations/Lepin Planet.md)

Also see the [catgories](Categories/Categories.md) for more information.

